export default {
    thermostats: 'Thermostats',
    empty_list: 'Nothing left in the list.',
    week: {
        mo: 'Mo',
        tu: 'Tu',
        we: 'We',
        th: 'Th',
        fr: 'Fr',
        sa: 'Sa',
        su: 'Su',
    },
    modes:{
        normal: 'Normal',
        eco: 'Eco',
        off: 'Off'
    },
    current_mode: 'Current mode',
    current_temp: 'Current temperature',

};