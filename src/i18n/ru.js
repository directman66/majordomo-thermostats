export default {
    thermostats: 'Термостаты',
    empty_list: 'Термостатов не найдено',
    week: {
        mo: 'Пн',
        tu: 'Вт',
        we: 'Ср',
        th: 'Чт',
        fr: 'Пт',
        sa: 'Сб',
        su: 'Вс',
    },
    modes:{
        normal: 'Обычный',
        eco: 'Эко',
        off: 'Выкл'
    },
    current_mode: 'Текущий режим',
    current_temp: 'Текущая температура',

};