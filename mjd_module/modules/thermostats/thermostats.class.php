<?php
/**
* Thermostats 
* @package project
* @author Wizard <sergejey@gmail.com>
* @copyright http://majordomo.smartliving.ru/ (c)
* @version 0.1 (wizard, 13:04:09 [Apr 01, 2019])
*/
//
//
class thermostats extends module {
/**
* thermostats
*
* Module class constructor
*
* @access private
*/
function __construct() {
  $this->name="thermostats";
  $this->title="Thermostats";
  $this->module_category="<#LANG_SECTION_APPLICATIONS#>";
  $this->checkInstalled();
}
/**
* saveParams
*
* Saving module parameters
*
* @access public
*/
function saveParams($data=1) {
 $p=array();
 if (IsSet($this->id)) {
  $p["id"]=$this->id;
 }
 if (IsSet($this->view_mode)) {
  $p["view_mode"]=$this->view_mode;
 }
 if (IsSet($this->edit_mode)) {
  $p["edit_mode"]=$this->edit_mode;
 }
 if (IsSet($this->tab)) {
  $p["tab"]=$this->tab;
 }
 return parent::saveParams($p);
}
/**
* getParams
*
* Getting module parameters from query string
*
* @access public
*/
function getParams() {
  global $id;
  global $mode;
  global $view_mode;
  global $edit_mode;
  global $tab;
  if (isset($id)) {
   $this->id=$id;
  }
  if (isset($mode)) {
   $this->mode=$mode;
  }
  if (isset($view_mode)) {
   $this->view_mode=$view_mode;
  }
  if (isset($edit_mode)) {
   $this->edit_mode=$edit_mode;
  }
  if (isset($tab)) {
   $this->tab=$tab;
  }
}
/**
* Run
*
* Description
*
* @access public
*/
function run() {
 global $session;
  $out=array();
  if ($this->action=='admin') {
   $this->admin($out);
  } else {
   $this->usual($out);
  }
  if (IsSet($this->owner->action)) {
   $out['PARENT_ACTION']=$this->owner->action;
  }
  if (IsSet($this->owner->name)) {
   $out['PARENT_NAME']=$this->owner->name;
  }
  $out['VIEW_MODE']=$this->view_mode;
  $out['EDIT_MODE']=$this->edit_mode;
  $out['MODE']=$this->mode;
  $out['ACTION']=$this->action;
  $this->data=$out;
  $p=new parser(DIR_TEMPLATES.$this->name."/".$this->name.".html", $this->data, $this);
  $this->result=$p->result;
}
/**
* BackEnd
*
* Module backend
*
* @access public
*/
function admin(&$out) {
    global $getConfig;
    if ($getConfig)
    {
            header("HTTP/1.0: 200 OK\n");
            header('Content-Type: text/html; charset=utf-8');
            $res = [];
            $res['lang'] = SETTINGS_SITE_LANGUAGE;
            echo json_encode($res);
            exit;
    }
    global $getTaskShedule;
    if ($getTaskShedule)
    {
            global $device;
            $point=SQLSelect("SELECT * FROM devices_scheduler_points WHERE DEVICE_ID=".(int)$device);
            
            header("HTTP/1.0: 200 OK\n");
            header('Content-Type: text/html; charset=utf-8');
            $res = [];
            
            $res[] = $this->getTimeDay($point,'1');
            $res[] = $this->getTimeDay($point,'2');
            $res[] = $this->getTimeDay($point,'3');
            $res[] = $this->getTimeDay($point,'4');
            $res[] = $this->getTimeDay($point,'5');
            $res[] = $this->getTimeDay($point,'6');
            $res[] = $this->getTimeDay($point,'0');
            echo json_encode($res);
            exit;
    }
    global $setTaskShedule;
    if ($setTaskShedule)
    {
        global $device;
        SQLExec("DELETE FROM devices_scheduler_points WHERE DEVICE_ID=".(int)$device);
        global $data;
        $data = json_decode($data, true);
        for ($i = 0; $i < 7; $i++)
        {
            $day = $data[$i];
            //echo $day;
            if ($day[0] != $day[1])
            {
                $rec = [];
                $rec["DEVICE_ID"] = $device;
                $rec["LINKED_METHOD"] = "turnOn";
                $c = $i+1;
                if ($c==7)$c=0;
                $rec["SET_DAYS"] = $c;
                $rec["ACTIVE"] = 1;
                $rec["SET_TIME"] = $this->getTimeStr($day[0]);
                SQLInsert('devices_scheduler_points',$rec);
                $rec = [];
                $rec["DEVICE_ID"] = $device;
                $rec["LINKED_METHOD"] = "turnOff";
                $c = $i+1;
                if ($c==7)$c=0;
                $rec["SET_DAYS"] = $c;
                $rec["ACTIVE"] = 1;
                $rec["SET_TIME"] = $this->getTimeStr($day[1]);
                SQLInsert('devices_scheduler_points',$rec);
            }
        }
        
        
        header("HTTP/1.0: 200 OK\n");
        header('Content-Type: text/html; charset=utf-8');
        echo "Ok";
        exit;
    }
}

function getTimeStr($t)
{
    $h = (int)($t / 6);
    $m = ($t % 6) * 10;
    return $h.":".str_pad($m, 2, '0', STR_PAD_LEFT);
}


function getTimeDay($point, $day)
{
    $mon_on = array_filter($point, function($v, $k) use($day){
                        $run_days = explode(',', $v['SET_DAYS']);
                        //echo print_r($run_days);
                        return in_array($day, $run_days) && $v['LINKED_METHOD']=="turnOn";
                    }, ARRAY_FILTER_USE_BOTH);
    $mon_off = array_filter($point, function($v, $k) use($day){
                        $run_days = explode(',', $v['SET_DAYS']);
                        return in_array($day, $run_days) && $v['LINKED_METHOD']=="turnOff";
                    }, ARRAY_FILTER_USE_BOTH);
    //echo print_r($point);
    //echo print_r($mon_on);
    //echo print_r($mon_off);
    $mon1 = 0;
    $mon2 = 0;
    if ($mon_on)
        $mon1 = $this->getTime(array_values($mon_on)[0]['SET_TIME']);
    if ($mon_off)
        $mon2 = $this->getTime(array_values($mon_off)[0]['SET_TIME']);
            
    return array($mon1,$mon2);
}

function getTime($time)
{
    //echo $time;
    $t = explode(':', $time);
    $res = $t[0] * 6 + $t[1] % 10;
    //echo $res;
    return $res;
}
/**
* FrontEnd
*
* Module frontend
*
* @access public
*/
function usual(&$out) {
 $this->admin($out);
}
/**
* Install
*
* Module installation routine
*
* @access private
*/
 function install($data='') {
  parent::install();
 }
// --------------------------------------------------------------------
}
/*
*
* TW9kdWxlIGNyZWF0ZWQgQXByIDAxLCAyMDE5IHVzaW5nIFNlcmdlIEouIHdpemFyZCAoQWN0aXZlVW5pdCBJbmMgd3d3LmFjdGl2ZXVuaXQuY29tKQ==
*
*/
